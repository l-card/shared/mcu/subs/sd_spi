/*
 * spi_sd.c
 *
 *  Created on: 20.02.2011
 *      Author: Melkor
 */

#include "LPC17xx.h"
#include "lpc17xx_pinfunc.h"
#include "iolpc17xx.h"
#include "sd_spi_config.h"
#include "sd_spi_port.h"
#include "system_LPC17xx.h"


#define SD_PORT_SPI_CLK  SystemCoreClock


/**********************************************************************************
 *  Инициализация SPI порта (используется SSP1 в режиме SPI)
 *  Клок выставляется равным системному (делитель 1)
 *  Импользуются 6 и 7 каналы GPDMA
 *********************************************************************************/
void sd_spi_port_init(void)
{
    LPC_SC->PCLKSEL0 = (LPC_SC->PCLKSEL0 & ~LPC_SC_PCLKSEL0_PCLK_SSP1_Msk) |
                (1 << LPC_SC_PCLKSEL0_PCLK_SSP1_Pos); //cclk/1
    LPC_SC->PCONP  |= LPC_SC_PCONP_PCSSP1_Msk;

    LPC_PINCON->PINSEL0 = (LPC_PINCON->PINSEL0 &
            ~(LPC_PINCON_PINSEL_PX6_Msk | LPC_PINCON_PINSEL_PX7_Msk |
                LPC_PINCON_PINSEL_PX8_Msk | LPC_PINCON_PINSEL_PX9_Msk))
                |  LPC_PINFUNC_P0_7_SCK1 | LPC_PINFUNC_P0_8_MISO1 | LPC_PINFUNC_P0_9_MOSI1;


    LPC_SSP1->CR0 = (0 << LPC_SSP_CR0_SCR_Pos) | LPC_SSP_CR0_CPHA_Msk | LPC_SSP_CR0_CPOL_Msk | (7 << LPC_SSP_CR0_DSS_Pos);
    LPC_SSP1->CR1 = LPC_SSP_CR1_SSE_Msk;


    SD_UNSELECT();
    LPC_GPIO0->FIODIR |= (1<<5);

    sd_spi_port_set_clk(400000);


    LPC_GPDMA->DMACConfig = LPC_GPDMA_DMACConfig_E_Msk;
    LPC_GPDMACH6->DMACCConfig = (2 << LPC_GPDMACH_DMACCConfig_DestPeripheral_Pos) |
            (1 << LPC_GPDMACH_DMACCConfig_TransferType_Pos) | (1<<14) | (1<<15);
    LPC_GPDMACH7->DMACCConfig = (3 << LPC_GPDMACH_DMACCConfig_SrcPeripheral_Pos) |
            (2 << LPC_GPDMACH_DMACCConfig_TransferType_Pos) | (1<<14) | (1<<15);

    sd_spi_port_rdwr(0xFF);
}


/***********************************************************************
 *  Установка клока SPI порта - ближайшее число, которое меньше hz Герц
 ***********************************************************************/
void sd_spi_port_set_clk(uint32_t hz)
{
    int div = SD_PORT_SPI_CLK/hz;

    //если не делится на цело, то делитель должен быть больше
    //(чтобы клок не привышал)
    if (SD_PORT_SPI_CLK!=div*hz)
        div++;

    //делитель должен быть четным для LPC17xx
    if (div&1)
        div++;

    LPC_SSP1->CPSR = div;
}


/************************************************************************
 * Запись, и одновременное чтение, одного байта по SPI порту (без dma)
 *
 ************************************************************************/
uint8_t sd_spi_port_rdwr(uint8_t val)
{
    LPC_SSP1->DR = val;
    while (!(LPC_SSP1->SR & LPC_SSP_SR_RNE_Msk)); //todo избавиться от цикла ожидания
    return LPC_SSP1->DR;
}


static volatile uint8_t f_spi_tx_ff = 0xFF;
static volatile uint8_t f_spi_rx_dummy;


/*************************************************************************************
 *  Запуск dma по spi порту.
 *  Параметры
 *     send_buf - указатель на буфер с данными для передачи
 *                если интересует только прием, то равен 0
 *     recv_buf - указатель на буфер, куда будут сохранены принятые данные
 *                если интересует только передача, то равен 0
 *     size     - размер dma в байтах
 *************************************************************************************/
void  sd_spi_port_dma_start(const uint8_t *send_buf, uint8_t *recv_buf, uint32_t size)
{
    LPC_SSP1->CR1 = 0;

    LPC_GPDMA->DMACIntErrClr = (1<<6) | (1<<7);
    LPC_GPDMA->DMACIntTCClear = (1<<6) | (1<<7);


    LPC_GPDMACH6->DMACCControl = ((size << LPC_GPDMACH_DMACCControl_TransferSize_Pos) &
            LPC_GPDMACH_DMACCControl_TransferSize_Msk) | LPC_GPDMACH_DMACCControl_I_Msk
            | (send_buf ? LPC_GPDMACH_DMACCControl_SI_Msk : 0);
    LPC_GPDMACH7->DMACCControl = ((size << LPC_GPDMACH_DMACCControl_TransferSize_Pos) &
                LPC_GPDMACH_DMACCControl_TransferSize_Msk) | LPC_GPDMACH_DMACCControl_I_Msk
                | (recv_buf ? LPC_GPDMACH_DMACCControl_DI_Msk : 0);
    LPC_GPDMACH6->DMACCLLI = 0;
    LPC_GPDMACH7->DMACCLLI = 0;
    LPC_GPDMACH6->DMACCDestAddr = (uint32_t)(&LPC_SSP1->DR);
    LPC_GPDMACH7->DMACCSrcAddr = (uint32_t)(&LPC_SSP1->DR);

    LPC_GPDMACH6->DMACCSrcAddr = (send_buf) ? (uint32_t)send_buf : (uint32_t)&f_spi_tx_ff;
    LPC_GPDMACH7->DMACCDestAddr = (recv_buf) ? (uint32_t)recv_buf : (uint32_t)&f_spi_rx_dummy;


    LPC_GPDMACH6->DMACCConfig |= LPC_GPDMACH_DMACCConfig_E_Msk;
    LPC_GPDMACH7->DMACCConfig |= LPC_GPDMACH_DMACCConfig_E_Msk;


    LPC_SSP1->DMACR = LPC_SSP_DMACR_RXDMAE_Msk | LPC_SSP_DMACR_TXDMAE_Msk;

    LPC_SSP1->CR1 = LPC_SSP_CR1_SSE_Msk;

  //  NVIC_EnableIRQ( DMA_IRQn );
}


/************************************************************************************************
 *  Проверка, завершилась ли последняя операция по dma порту
 *  0 - операция в процессе выполнения
 *  1 - завершилась
 ************************************************************************************************/
int sd_spi_port_dma_is_cpl(void)
{
    int cpl = (LPC_GPDMACH6->DMACCConfig & LPC_GPDMACH_DMACCConfig_E_Msk) ||
            (LPC_GPDMACH7->DMACCConfig & LPC_GPDMACH_DMACCConfig_E_Msk) ? 0 : 1;
    if (cpl)
        LPC_SSP1->DMACR = 0;
     return cpl;
}

