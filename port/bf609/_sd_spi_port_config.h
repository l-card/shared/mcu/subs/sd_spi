#ifndef SD_SPI_PORT_CONFIG_H_
#define SD_SPI_PORT_CONFIG_H_

/* ножка для выбора CS */
#define SD_SPI_CS_PIN          CHIP_PIN_PC14_GPIO
/* номер порта SPI (0 или 1) */
#define SD_SPI_PORT_NUM 1

#endif /* SD_SPI_PORT_CONFIG_H_ */
