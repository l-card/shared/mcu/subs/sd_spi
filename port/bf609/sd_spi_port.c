/*
 * spi_sd.c
 *
 *  Created on: 20.02.2011
 *      Author: Melkor
 */

#include "sd_spi_config.h"
#include "sd_spi_port.h"




#define REG_SD_SPI_CONCAT_(_port, _reg)       (pREG_SPI ## _port ## _ ## _reg)
#define REG_SD_SPI_CONCAT(_port, _reg) REG_SD_SPI_CONCAT_(_port, _reg)
#define pREG_SD_SPI(_reg)  REG_SD_SPI_CONCAT(SD_SPI_PORT_NUM, _reg)





/**********************************************************************************
 *  Инициализация SPI порта (используется SSP1 в режиме SPI)
 *  Клок выставляется равным системному (делитель 1)
 *  Импользуются 6 и 7 каналы GPDMA
 *********************************************************************************/
void sd_spi_port_init(void) {
    SD_UNSELECT();
    CHIP_PIN_CONFIG(SD_SPI_CS_PIN);
    CHIP_PIN_DIR_OUT(SD_SPI_CS_PIN);
#if SD_SPI_PORT_NUM == 0
    CHIP_PIN_CONFIG(CHIP_PIN_PD02_SPI0_MISO);
    CHIP_PIN_CONFIG(CHIP_PIN_PD03_SPI0_MOSI);
    CHIP_PIN_CONFIG(CHIP_PIN_PD04_SPI0_CLK);
#elif SD_SPI_PORT_NUM == 1
    CHIP_PIN_CONFIG(CHIP_PIN_PD14_SPI1_MISO);
    CHIP_PIN_CONFIG(CHIP_PIN_PD13_SPI1_MOSI);
    CHIP_PIN_CONFIG(CHIP_PIN_PD05_SPI1_CLK);
#else
    #error invalid SD_SPI_PORT_NUM specified
#endif


    *pREG_SD_SPI(CTL) = 0;
    *pREG_SD_SPI(SLVSEL) = 0;
    *pREG_SD_SPI(RXCTL) = BITM_SPI_RXCTL_REN;
    *pREG_SD_SPI(TXCTL) = BITM_SPI_TXCTL_TTI | BITM_SPI_TXCTL_TEN;

    sd_spi_port_set_clk(400000);
    sd_spi_port_rdwr(0xFF);
}


/***********************************************************************
 *  Установка клока SPI порта - ближайшее число, которое меньше hz Герц
 ***********************************************************************/
void sd_spi_port_set_clk(uint32_t hz) {
    *pREG_SD_SPI(CTL) = 0;
    *pREG_SD_SPI(CLK) = ((CHIP_CLKF_SCLK1 + (hz) - 1)/(hz) - 1);

    *pREG_SD_SPI(CTL) = BITM_SPI_CTL_CPOL | BITM_SPI_CTL_CPHA | BITM_SPI_CTL_EN | BITM_SPI_CTL_MSTR;
}


/************************************************************************
 * Запись, и одновременное чтение, одного байта по SPI порту (без dma)
 *
 ************************************************************************/
uint8_t sd_spi_port_rdwr(uint8_t val) {
    *pREG_SD_SPI(TFIFO) = val;
    while (*pREG_SD_SPI(STAT) & BITM_SPI_STAT_RFE) {}
    return *pREG_SD_SPI(RFIFO);
}


#if 0
static volatile uint8_t f_spi_tx_ff = 0xFF;
static volatile uint8_t f_spi_rx_dummy;
#endif


/*************************************************************************************
 *  Запуск dma по spi порту.
 *  Параметры
 *     send_buf - указатель на буфер с данными для передачи
 *                если интересует только прием, то равен 0
 *     recv_buf - указатель на буфер, куда будут сохранены принятые данные
 *                если интересует только передача, то равен 0
 *     size     - размер dma в байтах
 *************************************************************************************/
void  sd_spi_port_dma_start(const uint8_t *send_buf, uint8_t *recv_buf, uint32_t size) {
    /* сейчас для упрощения не используем dma, а копируем по байтам... */
    for (uint32_t i=0; i < size; i++) {
        uint8_t val = sd_spi_port_rdwr(send_buf == NULL ? 0xFF : *send_buf++);
        if (recv_buf)
            *recv_buf++ = val;
    }
}


/************************************************************************************************
 *  Проверка, завершилась ли последняя операция по dma порту
 *  0 - операция в процессе выполнения
 *  1 - завершилась
 ************************************************************************************************/
int sd_spi_port_dma_is_cpl(void) {
    return 1;
}

