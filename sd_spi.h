/*
 * sd.h
 *
 *  Created on: 28.12.2010
 *      Author: Borisov
 */

#ifndef SD_H_
#define SD_H_

#include "lcspec.h"
#include <stdint.h>

//ошибки
#define SD_ERR_INVALID_TOKEN        (-1)
#define SD_ERR_CMD_RESPONSE          (-2)
#define SD_ERR_BUSY_TIMEOUT          (-3)

/* 512 bytes for each sector */
#define SD_SECTOR_SIZE	512

//команды
#define SD_CMD0_GO_IDLE_STATE        (0x40 + 0)
#define SD_CMD8_SEND_IF_COND         (0x40 + 8)
#define SD_CMD9_SEND_CSD             (0x40 + 9)
#define SD_CMD10_SEND_CID            (0x40 + 10)
#define SD_CMD12_STOP_TRANSMISSION   (0x40 + 12)
#define SD_CMD17_READ_SINGLE_BLOCK   (0x40 + 17)
#define SD_CMD18_READ_MULTIPLE_BLOCK (0x40 + 18)
#define SD_CMD24_WRITE_BLOCK         (0x40 + 24)
#define SD_CMD25_WRITE_MULTIPLE_BLOCK (0x40 + 25)
#define SD_CMD55_APP_CMD             (0x40 + 55)
#define SD_CMD58_READ_OCR            (0x40 + 58)
#define SD_ACMD13_SD_STATUS		 	       (0xC0 + 13)
#define SD_ACMD23_SET_WR_BLK_ERASE_COUNT   (0xC0 + 23)
#define SD_ACMD41_SD_SEND_OP_COND          (0xC0 + 41)



#define SD_START_TOKEN              (0xFE)
#define SD_START_MWR_TOKEN          (0xFC)
#define SD_STOP_MWR_TOKEN           (0xFD)
#define SD_IS_ERR_TOKEN(_byte)      (!(_byte & 0xFF00))


//response bits in spi mode
#define SD_SPI_R1_IDLE               (0x01)
#define SD_SPI_R1_ERASE_RESET        (0x02)
#define SD_SPI_R1_ILLEGAL_CMD        (0x04)
#define SD_SPI_R1_CRC_ERR            (0x08)
#define SD_SPI_R1_ERASE_SEQ_ERR      (0x10)
#define SD_SPI_R1_ADDR_ERR           (0x20)
#define SD_SPI_R1_PARAM_ERR          (0x40)

//ответ на команду SEND_STATUS
#define SD_STATUS_LOCKED             (0x01)
#define SD_STATUS_WP_ERASE_SKIP      (0x02)
#define SD_STATUS_ERROR              (0x04)
#define SD_STATUS_CC_ERR             (0x08)
#define SD_STATUS_ECC_FAILD          (0x10)
#define SD_STATUS_WP_VIOLATION       (0x20)
#define SD_STATUS_ERASE_PARAM        (0x40)
#define SD_STATUS_OUT_OF_RANGE       (0x80)



#define SD_ACMD41_ARGS_HCS       (0x1 << 30)
#define SD_ACMD41_ARGS_XPC       (0x1 << 28)
#define SD_ACMD41_ARGS_S18R      (0x1 << 24)



// биты регистра OCR
#define SD_OCR_POWUP_STATUS  (0x1UL << 31)
#define SD_OCR_CCS           (0x1UL << 30)
#define SD_OCR_S18A          (0x1UL << 24)
#define SD_OCR_V35_36        (0x1UL << 23)
#define SD_OCR_V34_35        (0x1UL << 22)
#define SD_OCR_V33_34        (0x1UL << 21)
#define SD_OCR_V32_33        (0x1UL << 20)
#define SD_OCR_V31_32        (0x1UL << 19)
#define SD_OCR_V30_31        (0x1UL << 18)
#define SD_OCR_V29_30        (0x1UL << 17)
#define SD_OCR_V28_29        (0x1UL << 16)
#define SD_OCR_V27_28        (0x1UL << 15)
#define SD_OCR_LVR           (0x1UL << 7)



#include "lcspec_pack_start.h"
struct st_sd_cid {
	uint8_t  MID;     //Manufacturer ID
	uint8_t  OID[2];     //OEM/Application ID
	uint8_t  PNM[5];  //Product name
	uint8_t  PRV;    //Product revision
	uint32_t PSN;    //Product serial number
	uint16_t MDT;   //Manufacturing date
	uint8_t CRC;
} LATTRIBUTE_PACKED;
typedef struct st_sd_cid t_sd_cid;

struct st_sd_cfg {
    uint8_t present;
    uint32_t ocr;
    uint8_t csd[16];
    t_sd_cid cid;
    uint8_t stat[64];
    uint32_t sector_cnt;        //
    uint32_t sector_size;   // 512
    uint8_t type;
    uint8_t req_pending;
    uint32_t size;          //   size=sectorsize*sectorcnt
    uint32_t block_size;        // erase block size
} LATTRIBUTE_PACKED;
typedef struct st_sd_cfg t_sd_cfg;

#include "lcspec_pack_restore.h"

#define SD_TASK_DIR_RX   0
#define SD_TASK_DIR_TX   1

typedef enum {SD_TASK_STATE_FREE, SD_TASK_STATE_WAIT,
              SD_TASK_STATE_READ_CMD, SD_TASK_STATE_READ_DATA, SD_TASK_STATE_READ_STOP,
              SD_TASK_STATE_WRITE_CMD, SD_TASK_STATE_WRITE_DATA, SD_TASK_STATE_ERASE_CMD,
              SD_TASK_STATE_DONE, SD_TASK_WT_BURST_PACKET, SD_TASK_STATE_ERROR} t_sd_taks_state;


#define SD_INVALID_TASK 0xFF


void sd_init(void);
void sd_progress(void);
const t_sd_cfg *sd_get_cfg(uint8_t lun);

uint8_t sd_start_read_sectors (uint32_t sector, uint8_t *buff, uint32_t count);
uint8_t sd_read_burst_start (uint32_t sector, uint8_t *buff, uint32_t count, uint32_t full_count);
int32_t sd_read_burst_next (uint8_t task, uint8_t *buff, uint32_t count);

uint8_t sd_start_write_sectors (uint32_t sector, const uint8_t *buff, uint32_t count);
uint8_t sd_write_burst_start (uint32_t sector, const uint8_t *buff, uint32_t count, uint32_t full_count);
int32_t sd_write_burst_next (uint8_t task, const uint8_t *buff, uint32_t count);

int32_t sd_check_task_done (uint8_t task);





#endif /* SD_H_ */
