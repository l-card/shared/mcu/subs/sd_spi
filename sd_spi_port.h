/*
 * spi_sd.h
 *
 *  Created on: 20.02.2011
 *      Author: Melkor
 */

#ifndef SDSPI_PORT_H_
#define SDSPI_PORT_H_

#include <stdint.h>

void sd_spi_port_init(void);
void sd_spi_port_set_clk(uint32_t hz);
uint8_t sd_spi_port_rdwr(uint8_t val);

void sd_spi_port_dma_start(const uint8_t *send_buf, uint8_t *recv_buf, uint32_t size);
int sd_spi_port_dma_is_cpl(void);




#endif /* SPI_SD_H_ */
