/*
 * e124_sd.c
 *
 *  Created on: 27.12.2010
 *      Author: Borisov
 */


#include "ltimer.h"
#include "sd_spi_config.h"
#include "sd_spi_port.h"
#include "sd_spi.h"
#include <string.h>

#define SD_TYPE_FLG_NONE  0x0
#define SD_TYPE_FLG_MMC   0x1
#define SD_TYPE_FLG_SD1   0x2 //Sd Card Ver 1
#define SD_TYPE_FLG_SD2   0x4 //Sd Card Ver 2 or 3
#define SD_TYPE_FLG_BLOCK 0x8 //SDHC | SDXC
#define SD_TYPE_FLG_SDC   (SD_TYPE_FLG_SD1 | SD_TYPE_FLG_SD2)


#define SD_BACKGROUND

typedef struct {
    uint8_t dir;
    uint8_t burst;
    uint8_t *buf;
    uint8_t *cur_buf;
    uint32_t size;
    uint32_t proc_size;
    uint32_t full_size;
    uint32_t lba;
    t_sd_taks_state state;
} t_sd_task;


static struct {
    enum { SD_STATE_NOCARD, SD_STATE_CONFIG, SD_STATE_READY, SD_STATE_BUSY, SD_STATE_ERROR } state;
    enum { SD_CFGSTATE_INIT_WT,
        SD_CFGSTATE_CLKWT, //ждем пока закончится передача начальных клоков
        SD_CFGSTATE_C1_GO_IDLE, //ждем завершения начальной команды сброса
        SD_CFGSTATE_C2_IFCOND,
        SD_CFGSTATE_C3_WT_UP,
        SD_CFGSTATE_C4_OCR,
        SD_CFGSTATE_GET_CID_CMD,
        SD_CFGSTATE_GET_CID_DATA,
        SD_CFGSTATE_GET_CSD_CMD,
        SD_CFGSTATE_GET_CSD_DATA,
        SD_CFGSTATE_GET_STATUS_CMD,
        SD_CFGSTATE_GET_STATUS_DATA,

        SD_CFGSTATE_DONE,
        SD_CFGSTATE_ERROR
    } cfg_st;
    enum { SD_CMDSTATE_DONE,
        SD_ACMDSTATE_WTRDY,
        SD_ACMDSTATE_WTDMA,
        SD_CMDSTATE_WTRDY,
        SD_CMDSTATE_WTDMA
    } cmd_st;
    enum {
        SD_RDSTATE_WTTOKEN ,
        SD_RDSTATE_WTDATA,
        SD_RDSTATE_WTRDY,
        SD_RDSTATE_DONE,
        SD_RDSTATE_ERROR,
    } rd_st;

    uint8_t cmd;
    uint8_t cmd_rsp_pos;
    uint32_t arg;
    uint8_t cmd_resp;
    uint8_t cmd_rsp_size; //дополнительный размер ответа
                         //(сколько байт принять помимо первого ответа r1
    t_ltimer cmd_tmr;
    t_ltimer tmr; //таймер, используется для таймаута на выход из idle
                 //на прием стартого токена

    uint8_t* rdwr_buf;
    uint32_t rdwr_size;

    t_sd_cfg cfg;
    t_sd_task tasks[SD_TASK_CNT];
    uint8_t cur_task;
    uint8_t last_task;
} f_sd_st;


static uint8_t f_sd_tx_buf[20];
static uint8_t f_sd_rx_buf[20];

#define SD_CMD_SIZE          7
#define SD_CMD_SIZE_WTRESP  16




static void f_sd_start_cmd(uint8_t cmd, uint32_t arg, uint8_t rsp_size);
static int f_sd_proc_cmd(void);
static int f_sd_spi_rdy(void);



static void f_spi_release(void) {
    SD_UNSELECT();
    sd_spi_port_rdwr(0xFF);
}


/*****************************************************************************
 *  Запуск автомата чтения данных из sd-карты
 *  Переметры
 *    buff   - (in) буфер, куда будут сохранены данные
 *    size   - (in) размер данных для чтения
 *****************************************************************************/
static void f_sd_read_start(uint8_t* buff, uint32_t size) {
    ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_RDTOKEN_TOUT));
    f_sd_st.rd_st = SD_RDSTATE_WTTOKEN ;
    f_sd_st.rdwr_buf = buff;
    f_sd_st.rdwr_size = size;
}


/********************************************************************************
 * Продвижение автомата чтения данных из sd-карты
 * (до вызова этой функции должен быть вызван f_sd_read_start)
 * Возвращаемое значение
 *    0 - операция чтения еще в процессе
 *    1 - опереация чтения завершена успешно
 *    -1 - операция чтения завершена с ошибкой
 * Также успешность операции можно узнать по f_sd_st.rd_st (DONE или ERR)
 *********************************************************************************/
static int f_sd_read_proc(void) {
    int res = 0;
    if (f_sd_st.rd_st == SD_RDSTATE_WTTOKEN ) {
        //ждем, пока прочтем токен начала данных
        uint8_t token = sd_spi_port_rdwr(0xFF);
        if (token!=0xFF) {
            //проверка токена
            if (token != SD_START_TOKEN ) {
                f_sd_st.rd_st = SD_RDSTATE_ERROR;
                res = -1;
            } else {
                //запускаем dma на чтение
                sd_spi_port_dma_start(0, f_sd_st.rdwr_buf, f_sd_st.rdwr_size);
                f_sd_st.rd_st = SD_RDSTATE_WTDATA;
            }
        }
    } else if (f_sd_st.rd_st == SD_RDSTATE_WTDATA) {
        if (sd_spi_port_dma_is_cpl()) {
            //если dma завершен - читаем два байта - crc
            //xxx Проверка CRC при чтении
            sd_spi_port_rdwr(0xFF);                       /* Discard CRC */
            sd_spi_port_rdwr(0xFF);
            f_sd_st.rd_st = SD_RDSTATE_DONE;
            res = 1;
        }
    }
    return res;
}



/*******************************************************************
 *  Запуск автомата по записи блока данных на sd-карту
 *  Параметры:
 *     buff  - указатель на буфер, куда будут сохранены данные
 *             (размер всегда SD_SECTOR_SIZE)
 *     token - токен начала записи (SD_START_TOKEN для команды одиночной записи
 *             или SD_START_MWR_TOKEN для потоковой записи)
 *********************************************************************************/
static void f_sd_write_start (const uint8_t *buff, uint8_t token) {
    //пишем токен начала данных
    sd_spi_port_rdwr(token);       /* send data token first*/

    //запускаем dma для приема данных
    sd_spi_port_dma_start(buff, 0, SD_SECTOR_SIZE);
    f_sd_st.rd_st = SD_RDSTATE_WTDATA;
}

/************************************************************************************
 *  Продвижение автомата записи блока на sd-карту
 *  Возвращаемое значение
 *    0 - операция чтения еще в процессе
 *    1 - опереация чтения завершена успешно
 *    -1 - операция чтения завершена с ошибкой
 * Также успешность операции можно узнать по f_sd_st.rd_st (DONE или ERR)
 *************************************************************************************/
static int f_sd_write_proc (void) {
    int res = 0;

    if (f_sd_st.rd_st == SD_RDSTATE_WTDATA) {
        if (sd_spi_port_dma_is_cpl()) {
            uint8_t resp;
            //пишем crc...
            sd_spi_port_rdwr(0xFF);                 /* 16-bit CRC (Dummy) */
            sd_spi_port_rdwr(0xFF);

            //проверяем ответ
            resp = sd_spi_port_rdwr(0xFF);              /* Receive data response */
            if ((resp & 0x1F) != 0x05) {      /* If not accepted, return with error */
                f_sd_st.rd_st = SD_RDSTATE_ERROR;
                res = 1;
            } else {
                //ждем, пока завершится операция записи с таймаутом
                f_sd_st.rd_st = SD_RDSTATE_WTRDY;
                ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_RDY_TIMEOUT));
            }
        }
    } else if (f_sd_st.rd_st == SD_RDSTATE_WTRDY) {
        /* Wait while Flash Card is busy. */
        if (f_sd_spi_rdy()) {
            f_sd_st.rd_st = SD_RDSTATE_DONE;
            res = 1;
        } else if (ltimer_expired(&f_sd_st.tmr)) {
            f_sd_st.rd_st = SD_RDSTATE_ERROR;
            res = 1;
        }
    }
    return res;
}



static uint8_t f_get_next_task_in_state(t_sd_taks_state state) {
    uint8_t i, fnd = SD_INVALID_TASK;
    for (i=f_sd_st.last_task+1; (i < SD_TASK_CNT) && (fnd==SD_INVALID_TASK); i++) {
        if (f_sd_st.tasks[i].state == state)
            fnd = i;
    }

    for (i=0; (i < f_sd_st.last_task) && (i < SD_TASK_CNT) && (fnd==SD_INVALID_TASK); i++) {
        if (f_sd_st.tasks[i].state == state)
            fnd = i;
    }
    return fnd;
}

#define f_get_next_free_task() f_get_next_task_in_state(SD_TASK_STATE_FREE)
#define f_get_next_task()      f_get_next_task_in_state(SD_TASK_STATE_WAIT)


/*******************************************************************************
 *  Автомат, выполняющий задачи на чтение/запись
 *  В рабочем режиме вся работа с sd-картой идет через этот автомат
 ******************************************************************************/
static void f_sd_task_proc(void) {
    //если сейчас не выполняем никакой задачи - пробуем найти новую задачу
    if (f_sd_st.cur_task == SD_INVALID_TASK)
        f_sd_st.cur_task = f_get_next_task();

    //если есть задача (или нашли новую)
    if (f_sd_st.cur_task != SD_INVALID_TASK) {
        //сохраняем номер последней задачи, чтобы потом
        //искать следующую отностельно него
        f_sd_st.last_task = f_sd_st.cur_task;

        t_sd_task* task = &f_sd_st.tasks[(uint8_t)f_sd_st.cur_task];
        switch (task->state) {
            case SD_TASK_STATE_WAIT:
                //task->cur_buf = task->buf;
                //новая задача => анализируем направление передачи и размер
                if (task->dir == SD_TASK_DIR_RX) {
                    if (task->size == 1) {
                        //запускаем команду одиночного чтения
                        f_sd_start_cmd(SD_CMD17_READ_SINGLE_BLOCK, task->lba,0);
                    } else {
                        //запускаем команду непрерывного чтения
                        f_sd_start_cmd(SD_CMD18_READ_MULTIPLE_BLOCK, task->lba,0);
                    }
                    task->state = SD_TASK_STATE_READ_CMD;
                } else {
                    if (task->size == 1) {
                        //команда одиночной записи
                        f_sd_start_cmd(SD_CMD24_WRITE_BLOCK, task->lba,0);
                        task->state = SD_TASK_STATE_WRITE_CMD;
                    } else {
                        //если блочная запись, то вначале для эффективности
                        //подаем команду, указывающую сколько секторов нужно стереть
                        if (f_sd_st.cfg.type & SD_TYPE_FLG_SDC) {
                            f_sd_start_cmd(SD_ACMD23_SET_WR_BLK_ERASE_COUNT, task->full_size, 0);
                            task->state = SD_TASK_STATE_ERASE_CMD;
                        }
                    }

                }
                break;
            case SD_TASK_STATE_READ_CMD:
                if (f_sd_proc_cmd()) {
                    if (f_sd_st.cmd_resp==0) {
                        //читаем блок данных
                        f_sd_read_start(task->cur_buf, SD_SECTOR_SIZE);
                        task->state = SD_TASK_STATE_READ_DATA;
                    } else {
                        task->state = SD_TASK_STATE_ERROR;
                        f_spi_release();
                        f_sd_st.cur_task = SD_INVALID_TASK;
                    }
                }
                break;
            case SD_TASK_STATE_READ_DATA:
                if (f_sd_read_proc()) {
                    if (f_sd_st.rd_st == SD_RDSTATE_DONE) {
                        //завершили чтение всех блоков?
                        if (++task->proc_size == task->size) {
                            //если это пакетное чтение, то мб.  еще не нужно завершать
                            if (task->burst) {
                                task->full_size -= task->proc_size;
                                if (task->full_size != 0) {
                                    //ждем следующего блока данных
                                    ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_BURST_PACKET_TOUT));
                                    task->state = SD_TASK_WT_BURST_PACKET;
                                } else {
                                    //завершаем операцию - посылаем команду останова
                                    f_sd_start_cmd(SD_CMD12_STOP_TRANSMISSION, 0,0);
                                    task->state = SD_TASK_STATE_READ_STOP;                                    
                                }
                            } else {
                                //если была одиночное чтение, то можем отпустить spi
                                if (task->size == 1) {
                                    task->state = SD_TASK_STATE_DONE;
                                    f_spi_release();
                                    f_sd_st.cur_task = SD_INVALID_TASK;
                                } else {
                                    //иначе нужно еще подать команду окончания чтения
                                    f_sd_start_cmd(SD_CMD12_STOP_TRANSMISSION, 0,0);
                                    task->state = SD_TASK_STATE_READ_STOP;
                                }
                            }
                        } else {
                            //читаем следующий блок в задании
                            task->cur_buf += SD_SECTOR_SIZE;
                            f_sd_read_start(task->cur_buf, SD_SECTOR_SIZE);
                            task->state = SD_TASK_STATE_READ_DATA;
                        }
                    } else {
                        task->state = SD_TASK_STATE_ERROR;
                        f_spi_release();
                        f_sd_st.cur_task = SD_INVALID_TASK;
                    }
                }
                break;
            case SD_TASK_STATE_READ_STOP:
                if (f_sd_proc_cmd()) {
                    if (f_sd_st.cmd_resp==0) {
                        task->state = SD_TASK_STATE_DONE;
                    } else {
                        task->state = SD_TASK_STATE_ERROR;
                    }
                    f_spi_release();
                    f_sd_st.cur_task = SD_INVALID_TASK;
                }
                break;
            case SD_TASK_STATE_ERASE_CMD:
                if (f_sd_proc_cmd()) {
                    if (f_sd_st.cmd_resp==0) {
                        f_sd_start_cmd(SD_CMD25_WRITE_MULTIPLE_BLOCK, task->lba,0);
                        task->state = SD_TASK_STATE_WRITE_CMD;
                    } else {
                        task->state = SD_TASK_STATE_ERROR;                        
                        f_spi_release();
                        f_sd_st.cur_task = SD_INVALID_TASK;
                    }
                }
                break;
            case SD_TASK_STATE_WRITE_CMD:
                if (f_sd_proc_cmd()) {
                    if (f_sd_st.cmd_resp==0) {
                        f_sd_write_start(task->cur_buf, !task->burst && (task->size == 1) ? SD_START_TOKEN : SD_START_MWR_TOKEN);
                        task->state = SD_TASK_STATE_WRITE_DATA;
                    } else {
                        task->state = SD_TASK_STATE_ERROR;                        
                        f_spi_release();
                        f_sd_st.cur_task = SD_INVALID_TASK;
                    }
                }
                break;
            case SD_TASK_STATE_WRITE_DATA:
                if (f_sd_write_proc()) {
                    if (f_sd_st.rd_st == SD_RDSTATE_DONE) {
                        if (++task->proc_size == task->size) {
                            if (task->burst) {
                                task->full_size-= task->proc_size;
                                if (task->full_size != 0) {
                                    ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_BURST_PACKET_TOUT));
                                    task->state = SD_TASK_WT_BURST_PACKET;
                                } else {
                                    sd_spi_port_rdwr(SD_STOP_MWR_TOKEN);
                                    task->state = SD_TASK_STATE_DONE;
                                    f_spi_release();
                                    f_sd_st.cur_task = SD_INVALID_TASK;
                                }
                            } else {
                                if (task->size != 1) {
                                    sd_spi_port_rdwr(SD_STOP_MWR_TOKEN);
                                }
                                task->state = SD_TASK_STATE_DONE;
                                f_spi_release();
                                f_sd_st.cur_task = SD_INVALID_TASK;
                            }
                        } else {
                            task->cur_buf+=SD_SECTOR_SIZE;
                            f_sd_write_start(task->cur_buf, SD_START_MWR_TOKEN);
                            task->state = SD_TASK_STATE_WRITE_DATA;
                        }
                    } else {
                        task->state = SD_TASK_STATE_ERROR;
                        f_spi_release();
                        f_sd_st.cur_task = SD_INVALID_TASK;
                    }
                }
                break;
            case SD_TASK_WT_BURST_PACKET:
                if (ltimer_expired(&f_sd_st.tmr)) {
                    task->state = SD_TASK_STATE_ERROR;
                    f_spi_release();
                    f_sd_st.cur_task = SD_INVALID_TASK;
                }
            default:
                break;
        }
    }
}




static int f_sd_spi_rdy(void){
    return (sd_spi_port_rdwr (0xFF) == 0xFF) ? 1 : 0;
}




static void f_cmdresp_buf_move(void) {
    if (f_sd_st.cmd_rsp_size > 1) {
        memmove(&f_sd_rx_buf[SD_CMD_SIZE], &f_sd_rx_buf[SD_CMD_SIZE+1], f_sd_st.cmd_rsp_size - 1);
    }
    f_sd_rx_buf[SD_CMD_SIZE + f_sd_st.cmd_rsp_size - 1] = sd_spi_port_rdwr(0xFF);
}


/*********************************************************************
 * Продвижение автомата, выполняющего обработку команды
 * передачи команды по sd
 * При завершении команды (возвращает 1) ответ команды
 *   можно узнать в f_sd_st.cmd_resp
 *   (0xFF - недействительный ответ, свидетельствующий о ошибке)
 * Возвращаемое значение:
 *   0 - передача команды в процессе выполнения
 *   1 - передача команды закончена
 *******************************************************************/
static int f_sd_proc_cmd(void) {
    int i;

    switch (f_sd_st.cmd_st) {
        case SD_ACMDSTATE_WTRDY:
        case SD_CMDSTATE_WTRDY:
            //sd готова к передачи команды
            if (f_sd_spi_rdy()) {
                //заполняем буфер для передачи команды
                f_sd_tx_buf[0] = 0xFF;
                if (f_sd_st.cmd_st == SD_ACMDSTATE_WTRDY) {
                    f_sd_st.cmd_st = SD_ACMDSTATE_WTDMA;
                    f_sd_tx_buf[1] = SD_CMD55_APP_CMD;
                    f_sd_tx_buf[2] = 0;
                    f_sd_tx_buf[3] = 0;
                    f_sd_tx_buf[4] = 0;
                    f_sd_tx_buf[5] = 0;
                    f_sd_tx_buf[6] = 1;
                } else {
                    f_sd_st.cmd_st = SD_CMDSTATE_WTDMA;
                    f_sd_tx_buf[1] = f_sd_st.cmd;
                    f_sd_tx_buf[2] = (f_sd_st.arg >> 24) & 0xFF;
                    f_sd_tx_buf[3] = (f_sd_st.arg >> 16) & 0xFF;
                    f_sd_tx_buf[4] = (f_sd_st.arg >> 8) & 0xFF;
                    f_sd_tx_buf[5] = f_sd_st.arg & 0xFF;
                    /* Dummy CRC + Stop */
                    if (f_sd_st.cmd == SD_CMD0_GO_IDLE_STATE) {
                        f_sd_tx_buf[6] = 0x95;           /* Valid CRC for CMD0(0) */
                    } else if (f_sd_st.cmd == SD_CMD8_SEND_IF_COND) {
                        f_sd_tx_buf[6] = 0x87;
                    } else {
                        f_sd_tx_buf[6] = 1;
                    }
                }

                f_sd_st.cmd_rsp_size++;

                for (i=SD_CMD_SIZE; i < SD_CMD_SIZE + f_sd_st.cmd_rsp_size; i++) {
                    f_sd_tx_buf[i] = 0xFF;
                }
                sd_spi_port_dma_start(f_sd_tx_buf, f_sd_rx_buf,
                                      SD_CMD_SIZE + f_sd_st.cmd_rsp_size);
            } else if (ltimer_expired(&f_sd_st.cmd_tmr)) {
                f_sd_st.cmd_st = SD_CMDSTATE_DONE;
                /** @todo show error */
            }
            break;
        case SD_ACMDSTATE_WTDMA:
        case SD_CMDSTATE_WTDMA:
            if (sd_spi_port_dma_is_cpl()) {
                //ищем ответ - первый байт со старшим битом = 0
                if ((f_sd_st.cmd_st == SD_CMDSTATE_WTDMA) &&
                        (f_sd_st.cmd == SD_CMD12_STOP_TRANSMISSION)) {
                    f_cmdresp_buf_move();
                  //  if (f_sd_rx_buf[SD_CMD_SIZE] !=0)
                  //      f_cmdresp_buf_move();
                }

                for (i=0; (i < 55) && (f_sd_rx_buf[SD_CMD_SIZE] & 0x80); i++)
                    f_cmdresp_buf_move();


                if (f_sd_st.cmd_st == SD_ACMDSTATE_WTDMA) {
                    if (f_sd_rx_buf[SD_CMD_SIZE] & 0x80) {
                        f_sd_st.cmd_st = SD_CMDSTATE_DONE;
                        /** @todo timeout */
                    } else {
                        //закончили передавать acmd - передаем нужную команду
                        f_sd_start_cmd(f_sd_st.cmd, f_sd_st.arg, f_sd_st.cmd_rsp_size-1);
                    }
                } else {
                    f_sd_st.cmd_resp = f_sd_rx_buf[SD_CMD_SIZE];
                    f_sd_st.cmd_rsp_pos = SD_CMD_SIZE;
                    f_sd_st.cmd_st = SD_CMDSTATE_DONE;
                }
            }
            break;
        default:
            break;
    }

    return (f_sd_st.cmd_st == SD_CMDSTATE_DONE) ? 1 : 0;
}


/**************************************************************************
 *  запуск автомата, передающего команду sd по spi.
 *  (При вызове не должно идти других операций по spi!!)
 *  Для завершения команды, необходимо после  f_sd_start_cmd
 *  вызывать f_sd_proc_cmd, пока она не вернет 1
 *  Параметры:
 *     cmd      - (in) код команды (старший бит - признак ACMD команды)
 *     arg      - (in) аргумент команды для передачи
 *     rsp_size - (in) размер ответа - 1 (первое слово ответа r1 - принимается всегда)
 *
 ****************************************************************************/
static void f_sd_start_cmd(uint8_t cmd, uint32_t arg, uint8_t rsp_size) {
    f_sd_st.cmd = cmd & 0x7F;
    f_sd_st.arg = arg;
    //старший бит - признак ACMD - перед самой командой надо передать CMD55
    f_sd_st.cmd_st = (cmd & 0x80) ? SD_ACMDSTATE_WTRDY :  SD_CMDSTATE_WTRDY;
    f_sd_st.cmd_resp  = 0xFF;
    f_sd_st.cmd_rsp_size = rsp_size;

    //даем импульс на cs - признак новой команды
    SD_UNSELECT();
    SD_SELECT();
    //устанавливаем таймер на ожидание готовности карты
    ltimer_set(&f_sd_st.cmd_tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_RDY_TIMEOUT));

    f_sd_proc_cmd();
}

/******************************************************************************
 *  Запуск процесса получения конфигурации SD-карты
 *  после этого необходимо вызывать f_sd_cfg_proc до
 *  тех пор, пока f_sd_st.state остается SD_STATE_CONFIG
 *****************************************************************************/
static void f_sd_start_cfg(void) {
    SD_UNSELECT();
    f_sd_st.state = SD_STATE_CONFIG;
    f_sd_st.cfg_st = SD_CFGSTATE_INIT_WT;
    f_sd_st.cfg.type = 0;

    ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_INSERT_TOUT));
}


static void  f_sd_fill_size(t_sd_cfg* cfg) {
    uint16_t csize;
    cfg->sector_size = SD_SECTOR_SIZE;

    /* Get number of sectors on the disk (DWORD) */
    if ((cfg->csd[0] >> 6) == 1) /* SDC ver 2.00 */ {
        csize = cfg->csd[9] + ((uint16_t)cfg->csd[8] << 8) + 1;
        cfg -> sector_cnt = (uint32_t)csize << 10;
    } else {
        /* SDC ver 1.XX or MMC*/
        //mem = (c_size+1)*2^(c_size_mul+2)*2^(READ_BL_LEN)
        int n = (cfg->csd[5] & 15) + ((cfg->csd[10] & 128) >> 7) + ((cfg->csd[9] & 3) << 1) + 2;  // 19
        csize = (cfg->csd[8] >> 6) + ((uint16_t)cfg->csd[7] << 2) + ((uint16_t)(cfg->csd[6] & 3) << 10) + 1; // 3752
        cfg -> sector_cnt = (uint32_t)csize << (n - 9); // 3842048
    }

    cfg->size = cfg->sector_cnt * cfg->sector_size; // 512*3842048=1967128576Byte (1.83GB)

    /* Get erase block size in unit of sector (DWORD) */
    if (cfg->type & SD_TYPE_FLG_SD2) {
        /* SDC ver 2.00 */
        cfg->block_size = 14UL << (cfg->stat[10] >> 4); //AU Size  //16
    } else {
        /* SDC ver 1.XX or MMC */
        if (cfg->type & SD_TYPE_FLG_SD1) {
            /* SDC ver 1.XX */
            cfg->block_size = (((cfg->csd[10] & 63) << 1) + ((uint16_t)(cfg->csd[11] & 128) >> 7) + 1) << ((cfg->csd[13] >> 6) - 1);
        } else {
            /* MMC */
            // cfg->blocksize = ((uint16_t)((buf[10] & 124) >> 2) + 1) * (((buf[11] & 3) << 3) + ((buf[11] & 224) >> 5) + 1);
            cfg->block_size = ((uint16_t)((cfg->csd[10] & 124) >> 2) + 1) * (((cfg->csd[10] & 3) << 3) + ((cfg->csd[11] & 224) >> 5) + 1);
        }
    }
}


/******************************************************************************
 *  Продвижение автомата получения конфигурации SD-карты
 *  По завершению устанавивает f_sd_st.state в SD_STATE_RDY (успешно завершено)
 *  или SD_STATE_ERROR. В первом случае конфигурация сохраняется в структуре
 *  f_sd_st.cfg
 *****************************************************************************/
static void f_sd_cfg_proc(void) {
    switch (f_sd_st.cfg_st) {
        case SD_CFGSTATE_INIT_WT:
            if (ltimer_expired(&f_sd_st.tmr)) {
                /* At least 74 clock cycles are required prior to starting bus communication */
                sd_spi_port_dma_start(0, 0, 80);
                f_sd_st.cfg_st = SD_CFGSTATE_CLKWT;
            }
            break;
        case SD_CFGSTATE_CLKWT:
            if (sd_spi_port_dma_is_cpl()) {
                f_sd_st.cfg_st = SD_CFGSTATE_C1_GO_IDLE;
                f_sd_start_cmd(SD_CMD0_GO_IDLE_STATE, 0, 0);
            }
            break;
        case SD_CFGSTATE_C1_GO_IDLE:
            if (f_sd_proc_cmd()) {
                //завершилась команда начального сброса. проверяем ответ
                if (f_sd_st.cmd_resp == SD_SPI_R1_IDLE) {
                    f_sd_st.cfg_st = SD_CFGSTATE_C2_IFCOND;
                    f_sd_start_cmd(SD_CMD8_SEND_IF_COND, 0x1AA, 4);
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_C2_IFCOND:
            if (f_sd_proc_cmd()) {
                //завершилась команда CMD8
                if (f_sd_st.cmd_resp == SD_SPI_R1_IDLE) {
                    //пришел ответ => spec 2.0
                    //проверяем ответ
                    if ((f_sd_rx_buf[f_sd_st.cmd_rsp_pos+3] == 0x01)
                            && (f_sd_rx_buf[f_sd_st.cmd_rsp_pos+4] == 0xAA)) {
                        f_sd_st.cfg.type = SD_TYPE_FLG_SD2;
                        ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_LEAVING_IDLE_TIMEOUT));
                        f_sd_start_cmd(SD_ACMD41_SD_SEND_OP_COND,
                                SD_ACMD41_ARGS_HCS,0);
                        f_sd_st.cfg_st = SD_CFGSTATE_C3_WT_UP;
                    } else {
                        f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                    }
                } else {
                    //ответа нет => spec 1.0
                    //вообще она может быть и MMC, если на след. команду не ответит
                    f_sd_st.cfg.type = SD_TYPE_FLG_SD1;
                    ltimer_set(&f_sd_st.tmr, LTIMER_MS_TO_CLOCK_TICKS(SD_LEAVING_IDLE_TIMEOUT));
                    f_sd_start_cmd(SD_ACMD41_SD_SEND_OP_COND, 0,0);
                    f_sd_st.cfg_st = SD_CFGSTATE_C3_WT_UP;
                }
            }
            break;
        case SD_CFGSTATE_C3_WT_UP:
            if (f_sd_proc_cmd()) {
                if (f_sd_st.cmd_resp == 0) {
                    f_sd_st.cfg_st = SD_CFGSTATE_C4_OCR;
                    f_sd_start_cmd(SD_CMD58_READ_OCR, 0, 4);
                } else if (f_sd_st.cmd_resp==SD_SPI_R1_IDLE)/* || (f_sd_st.cmd_resp=0xFF)) */ {
                    //если не вышли из idle - посылаем еще команду
                    if (!ltimer_expired(&f_sd_st.tmr)) {
                        f_sd_start_cmd(SD_ACMD41_SD_SEND_OP_COND,
                                 (f_sd_st.cfg.type & SD_TYPE_FLG_SD2) ?
                                         SD_ACMD41_ARGS_HCS : 0,0);
                    } else {
                        f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                    }
                } else {
                    //если карта не отвечает на SD_ACMD41_SD_SEND_OP_COND
                    //это MMC карта, но тут она не поддерживается, так что error!
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR; //SD_CFGSTATE_C4_OCR
                    f_sd_st.cfg.type = SD_TYPE_FLG_MMC;
                }
            }
            break;
        case SD_CFGSTATE_C4_OCR:
            if (f_sd_proc_cmd()) {
                if (f_sd_st.cmd_resp==0) {
                    f_sd_st.cfg.ocr = f_sd_rx_buf[f_sd_st.cmd_rsp_pos+4];
                    f_sd_st.cfg.ocr|= (uint32_t)f_sd_rx_buf[f_sd_st.cmd_rsp_pos+3] << 8;
                    f_sd_st.cfg.ocr|= (uint32_t)f_sd_rx_buf[f_sd_st.cmd_rsp_pos+2] << 16;
                    f_sd_st.cfg.ocr|= (uint32_t)f_sd_rx_buf[f_sd_st.cmd_rsp_pos+1] << 24;

                    if (f_sd_st.cfg.ocr & SD_OCR_CCS)
                        f_sd_st.cfg.type |= SD_TYPE_FLG_BLOCK;

                    f_spi_release();
                    if (f_sd_st.cfg.type == SD_TYPE_FLG_MMC) {
                        sd_spi_port_set_clk(20000000);
                    } else {
                        sd_spi_port_set_clk(25000000);
                    }

                    f_sd_start_cmd(SD_CMD10_SEND_CID,0,0);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_CID_CMD;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_CID_CMD:
            if (f_sd_proc_cmd()) {
                if (f_sd_st.cmd_resp==0) {
                    f_sd_read_start((uint8_t*)&f_sd_st.cfg.cid, 16);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_CID_DATA;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_CID_DATA:
            if (f_sd_read_proc()) {
                if (f_sd_st.rd_st == SD_RDSTATE_DONE) {
                    f_sd_start_cmd(SD_CMD9_SEND_CSD,0,0);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_CSD_CMD;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_CSD_CMD:
            if (f_sd_proc_cmd()) {
                if (f_sd_st.cmd_resp==0) {
                    f_sd_read_start(f_sd_st.cfg.csd, 16);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_CSD_DATA;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_CSD_DATA:
            if (f_sd_read_proc()) {
                if (f_sd_st.rd_st == SD_RDSTATE_DONE) {
                    f_sd_start_cmd(SD_ACMD13_SD_STATUS,0,1);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_STATUS_CMD;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_STATUS_CMD:
            if (f_sd_proc_cmd()) {
                if (f_sd_st.cmd_resp==0) {
                    f_sd_read_start(f_sd_st.cfg.stat, 64);
                    f_sd_st.cfg_st = SD_CFGSTATE_GET_STATUS_DATA;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_GET_STATUS_DATA:
            if (f_sd_read_proc()) {
                if (f_sd_st.rd_st == SD_RDSTATE_DONE) {
                    f_sd_fill_size(&f_sd_st.cfg);
                    f_spi_release();
                    f_sd_st.cfg_st = SD_CFGSTATE_DONE;
                    f_sd_st.state = SD_STATE_READY;
                    f_sd_st.cfg.present = 1;
                    f_sd_st.cur_task = SD_INVALID_TASK;
                    f_sd_st.last_task = SD_INVALID_TASK;
                } else {
                    f_sd_st.cfg_st = SD_CFGSTATE_ERROR;
                }
            }
            break;
        case SD_CFGSTATE_ERROR:
            SD_SELECT();
            if (f_sd_spi_rdy()) {
                f_sd_st.state = SD_STATE_ERROR;
                SD_UNSELECT();
            }
            break;
        default:
            break;
    }

}



/*****************************************************
 *   Инициализация SPI-интерфейса и переменных состояния sd-автомата.
 *   Должна выполняться один раз до использования остальных функций
 ******************************************************/
void sd_init(void) {
    f_sd_st.state = SD_STATE_NOCARD;
    f_sd_st.cfg.present = 0;
    sd_spi_port_init();
}

const t_sd_cfg *sd_get_cfg(uint8_t lun) {
    return &f_sd_st.cfg;
}



/***********************************************************************
 *  Продвижение автомата управления sd-картой
 *
 ***********************************************************************/
void sd_progress(void) {
    int pres = SD_IS_PRESENT();

    if (!pres) {
        if (f_sd_st.state != SD_STATE_NOCARD) {
            f_sd_st.cfg.present = 0;
            f_sd_st.state = SD_STATE_NOCARD;
        }
    } else {
        switch (f_sd_st.state) {
            case SD_STATE_NOCARD:                
                f_sd_start_cfg();
                break;
            case SD_STATE_CONFIG:
                //идет процесс конфигурации SD-карты
                f_sd_cfg_proc();
                break;
            case SD_STATE_READY:
                f_sd_task_proc();
                break;
            default:
                break;
        }
    }
}


uint8_t sd_start_read_sectors (uint32_t sector, uint8_t *buff, uint32_t count) {
    uint8_t task = f_get_next_free_task();
    if (task!=SD_INVALID_TASK) {
        f_sd_st.tasks[task].burst = 0;
        f_sd_st.tasks[task].buf = f_sd_st.tasks[task].cur_buf = buff;
        f_sd_st.tasks[task].dir = SD_TASK_DIR_RX;
        f_sd_st.tasks[task].size = count;
        f_sd_st.tasks[task].lba = sector;
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].state = SD_TASK_STATE_WAIT;

        /* Convert to byte address if needed */
        if (!(f_sd_st.cfg.type & SD_TYPE_FLG_BLOCK))
            f_sd_st.tasks[task].lba *= SD_SECTOR_SIZE;
    }
    return task;
}

uint8_t sd_read_burst_start (uint32_t sector, uint8_t *buff,
                             uint32_t count, uint32_t full_count) {
    uint8_t task = f_get_next_free_task();
    if (task!=SD_INVALID_TASK) {
        f_sd_st.cur_task = task;
        f_sd_st.tasks[task].buf = f_sd_st.tasks[task].cur_buf = (uint8_t*)buff;
        f_sd_st.tasks[task].dir = SD_TASK_DIR_RX;
        f_sd_st.tasks[task].size = count;
        f_sd_st.tasks[task].lba = sector;
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].state = SD_TASK_STATE_WAIT;
        f_sd_st.tasks[task].burst = 1;
        f_sd_st.tasks[task].full_size = full_count;

        /* Convert to byte address if needed */
        if (!(f_sd_st.cfg.type & SD_TYPE_FLG_BLOCK))
            f_sd_st.tasks[task].lba *= SD_SECTOR_SIZE;
    }
    return task;
}

int32_t sd_read_burst_next (uint8_t task, uint8_t *buff, uint32_t count) {
    if (f_sd_st.tasks[task].state == SD_TASK_WT_BURST_PACKET) {
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].cur_buf = buff;
        f_sd_st.tasks[task].size = count;
        f_sd_read_start(buff, SD_SECTOR_SIZE);
        f_sd_st.tasks[task].state = SD_TASK_STATE_READ_DATA;
        return 0;
    }
    else return -1;
}

uint8_t sd_start_write_sectors (uint32_t sector, const uint8_t *buff, uint32_t count) {
    uint8_t task = f_get_next_free_task();
    if (task != SD_INVALID_TASK) {
        f_sd_st.cur_task = task;
        f_sd_st.tasks[task].buf = f_sd_st.tasks[task].cur_buf = (uint8_t*)buff;
        f_sd_st.tasks[task].dir = SD_TASK_DIR_TX;
        f_sd_st.tasks[task].size = count;
        f_sd_st.tasks[task].lba = sector;
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].state = SD_TASK_STATE_WAIT;
        f_sd_st.tasks[task].burst = 0;
        f_sd_st.tasks[task].full_size = count;


        /* Convert to byte address if needed */
        if (!(f_sd_st.cfg.type & SD_TYPE_FLG_BLOCK))
            f_sd_st.tasks[task].lba *= SD_SECTOR_SIZE;
    }

    return task;
}



uint8_t sd_write_burst_start (uint32_t sector, const uint8_t *buff,
                              uint32_t count, uint32_t full_count) {
    uint8_t task = f_get_next_free_task();
    if (task!=SD_INVALID_TASK) {
        f_sd_st.cur_task = task;
        f_sd_st.tasks[task].buf = f_sd_st.tasks[task].cur_buf = (uint8_t*)buff;
        f_sd_st.tasks[task].dir = SD_TASK_DIR_TX;
        f_sd_st.tasks[task].size = count;
        f_sd_st.tasks[task].lba = sector;
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].state = SD_TASK_STATE_WAIT;
        f_sd_st.tasks[task].burst = 1;
        f_sd_st.tasks[task].full_size = full_count;

        /* Convert to byte address if needed */
        if (!(f_sd_st.cfg.type & SD_TYPE_FLG_BLOCK))
            f_sd_st.tasks[task].lba *= SD_SECTOR_SIZE;
    }
    return task;
}

int32_t sd_write_burst_next (uint8_t task, const uint8_t *buff, uint32_t count) {
    if (f_sd_st.tasks[task].state == SD_TASK_WT_BURST_PACKET) {
        f_sd_st.tasks[task].proc_size = 0;
        f_sd_st.tasks[task].cur_buf = (uint8_t*)buff;
        f_sd_st.tasks[task].size = count;
        f_sd_write_start(buff, SD_START_MWR_TOKEN);
        f_sd_st.tasks[task].state = SD_TASK_STATE_WRITE_DATA;
        return 0;
    }
    else return -1;
}




int32_t sd_check_task_done (uint8_t task) {
    int32_t res = 1;
    if (f_sd_st.tasks[task].state == SD_TASK_STATE_DONE) {
        res = 0;
        f_sd_st.tasks[task].state = SD_TASK_STATE_FREE;
    } else if (f_sd_st.tasks[task].state ==SD_TASK_WT_BURST_PACKET) {
        res = 0;
    } else if (f_sd_st.tasks[task].state == SD_TASK_STATE_ERROR) {
        res = -1;
        f_sd_st.tasks[task].state = SD_TASK_STATE_FREE;
    }
    return res;
}
