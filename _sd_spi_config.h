#ifndef SD_SPI_CFG_H_
#define SD_SPI_CFG_H_

//макросы для управления cs для spi у sd-карты
#define SD_SELECT()            
#define SD_UNSELECT()          

//макрос для проверки, присутствует ли sd-карта
#define SD_IS_PRESENT()        


//таймауты
#define SD_RDY_TIMEOUT           2000
#define SD_LEAVING_IDLE_TIMEOUT  4000
#define SD_RDTOKEN_TOUT          200
#define SD_INSERT_TOUT           500
#define SD_BURST_PACKET_TOUT     4000


#define SD_TASK_CNT   4


#endif /* SD_SPI_CFG_H_ */
